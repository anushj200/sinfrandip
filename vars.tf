variable "image_id" {
  type = string
  default = "ami-02e136e904f3da870"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "vpc_cidr" {
  type = string
  default = "10.0.0.0/16"
}

variable "vpc_name" {
  type = string
  default = "exl-vpc"
}

variable "sg_name" {
  type = string
  default = "exlsg"
}

variable "ssh_keyname" {
  type = string
  default = "ndip-key"
}