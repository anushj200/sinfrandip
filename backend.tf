terraform {
  backend "s3" {
    bucket = "mybucket"
    key    = "infra/tfstate"
    region = "us-east-1"
  }
}