resource "aws_instance" "webserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[0]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "webserver"
  }
}

resource "aws_instance" "appserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "appserver"
  }
}

resource "aws_instance" "dbserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "dbserver"
  }
}

resource "aws_instance" "dataprocessingserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "dataprocessingserver"
  }
}

resource "aws_instance" "blockchainserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "blockchainserver"
  }
}

resource "aws_instance" "poswebserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "poswebserver"
  }
}

resource "aws_instance" "posappserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "posappserver"
  }
}

resource "aws_instance" "posdbserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "posdbserver"
  }
}

resource "aws_instance" "dbdatalakeserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "dbdatalakeserver"
  }
}

resource "aws_instance" "backendserver" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name = var.ssh_keyname
  subnet_id = module.vpc.private_subnets[1]
  vpc_security_group_ids = [aws_security_group.exlsg.id]

  tags = {
    Name = "backendserver"
  }
}