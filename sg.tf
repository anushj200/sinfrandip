resource "aws_security_group" "exlsg" {
  name        = var.sg_name
  description = "exl sg"
  vpc_id      = module.vpc.vpc_id

  tags = {
    Name = "exlsg"
  }
}